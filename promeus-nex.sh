#!bin/bash
#installer nginx
sudo apt update
sudo apt install tee curl nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx
# Create Prometheus Users
sudo useradd --no-create-home --shell /bin/false prome
sudo useradd --no-create-home --shell /bin/false node_exporter
sudo mkdir /etc/prometheus
sudo mkdir /var/lib/prometheus
#telecharger et installer prometheus
wget https://github.com/prometheus/prometheus/releases/download/v2.39.1/prometheus-2.39.1.darwin-amd64.tar.gz
tar xvf prometheus-2.39.1.darwin-amd64.tar.gz
sudo cp prometheus-2.39.1.darwin-amd64/prometheus /usr/local/bin/
sudo cp prometheus-2.39.1.darwin-amd64/promtool /usr/local/bin/
sudo chown prome:prome /usr/local/bin/prometheus
sudo chown prome:prome /usr/local/bin/promtool
sudo cp -r prometheus-2.39.1.darwin-amd64/consoles /etc/prometheus
sudo cp -r prometheus-2.39.1.darwin-amd64/console_libraries /etc/prometheus
#sudo chown -R prome:prome /etc/prometheus/consoles
#sudo chown -R prome:prome /etc/prometheus/console_libraries
#sudo chmod 777 /etc/prometheus/
#sudo rm -rf /etc/prometheus/prometheus.yml
#sudo cp -r prometheus.yml /etc/prometheus/
sudo tee /etc/prometheus/prometheus.yml <<EOF
# my global configue
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
      # - alertmanager:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'

    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.

    static_configs:
    - targets: ['localhost:9090']
EOF
sudo tee /etc/systemd/system/prometheus.service<<EOF
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/docs/introduction/overview/
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=prometheus
Group=prometheus
ExecReload=/bin/kill -HUP \$MAINPID
ExecStart=/usr/local/bin/prometheus \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/var/lib/prometheus \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries \
  --web.listen-address=0.0.0.0:9090 \
  --web.external-url=

SyslogIdentifier=prometheus
Restart=always

[Install]
WantedBy=multi-user.target
EOF
#sudo chmod 777 /etc/systemd/system/
#sudo rm -rf /etc/systemd/system/prometheus.service
#sudo cp -r prometheus.service /etc/systemd/system/
sudo chmod +x /etc/systemd/system/prometheus.service
sudo chown -R prome:prome /etc/prometheus/consoles
sudo chown -R prome:prome /etc/prometheus/console_libraries
sudo systemctl daemon-reload
#Start the Prometheus service :
sudo systemctl start prometheus
sudo systemctl enable prometheus
#Access the Prometheus Web Interface
echo "Pour le test faire , http://ip-address:9090"
#Add Exporters

##Node_exporter-
##Blackbox_exporter
##rabbitmq_exporter
##Mysqld_exporter
wget https://github.com/prometheus/node_exporter/releases/download/v0.15.1/node_exporter-0.15.1.linux-amd64.tar.gz
tar xvf node_exporter-0.15.1.linux-amd64.tar.gz
sudo cp node_exporter-0.15.1.linux-amd64/node_exporter /usr/local/bin
#sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
#sudo chmod 777 /etc/systemd/system/
#sudo rm -rf /etc/systemd/system/node_exporter.service
#sudo cp -r node_exporter.service /etc/systemd/system/
sudo tee /etc/systemd/system/node_exporter.service <<EOF
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User= node_exporter
Group= node_exporter
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
EOF
#sudo chmod +x /etc/systemd/system/node_exporter.service
#sudo cp node_exporter-0.15.1.linux-amd64/node_exporter /usr/local/bin
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter
sudo chmod 777 /etc/prometheus/
sudo rm -rf /etc/prometheus/prometheus.yml
sudo cp -r prometheus2.yml /etc/prometheus/
sudo mv -f prometheus2.yml /etc/prometheus/prometheus.yml
sudo chown -R prome:prome /etc/prometheus/consoles
sudo chown -R prome:prome /etc/prometheus/console_libraries
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl restart Prometheus
echo "Pour le test faire , http://ip-address:9090"
